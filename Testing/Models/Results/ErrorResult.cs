﻿// <copyright file="ErrorResult.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Models.Results
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Testing.Constants;
    using Testing.Models.Enums;
    using Testing.Utilities;

    /// <summary>
    /// This class contains an error result information.
    /// </summary>
    public class ErrorResult : AbstractResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorResult"/> class.
        /// </summary>
        /// <param name="ex">Exception thrown.</param>
        /// <param name="sourceName">Source name.</param>
        /// <param name="operation">Operation name.</param>
        public ErrorResult(Exception ex, MethodInfo sourceName, MethodType operation)
            : base(string.Format(InternalConstants.MessageFormat, sourceName.ReflectedType.Name, sourceName.Name))
        {
            this.Exception = ex.Message;
            this.Message = string.Format(MessageConstants.TargetInvocationMessage, Dictionaries.MethodTypeDictionary.GetValueOrDefault(operation));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorResult"/> class.
        /// </summary>
        /// <param name="ex">Exception thrown.</param>
        /// <param name="operation">Operation name.</param>
        public ErrorResult(Exception ex, MethodType operation)
            : base(string.Format(InternalConstants.MessageFormat, ex.TargetSite.DeclaringType.Name, ex.TargetSite.Name))
        {
            this.Exception = ex.Message;
            this.Message = string.Format(MessageConstants.TargetInvocationMessage, Dictionaries.MethodTypeDictionary.GetValueOrDefault(operation));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorResult"/> class.
        /// </summary>
        /// <param name="type">Class type where the instanciation has failed.</param>
        public ErrorResult(Type type)
            : base(string.Format(InternalConstants.MessageFormat, type.Name, type.GetConstructor(Type.EmptyTypes)))
        {
            this.Exception = MessageConstants.NonInvocableMessage;
            this.Message = string.Format(MessageConstants.TargetInvocationMessage, InternalConstants.InstanciateOperationName);
        }

        /// <summary>
        /// Gets the message value.
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Gets the exception message.
        /// </summary>
        public string Exception { get; }
    }
}
