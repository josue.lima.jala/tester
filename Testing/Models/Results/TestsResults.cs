﻿// <copyright file="TestsResults.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Models.Results
{
    using System.Collections.Generic;
    using Testing.Utilities;

    /// <summary>
    /// This class contains all performed tests general info.
    /// </summary>
    public class TestsResults
    {
        private readonly List<AbstractResult> results;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestsResults"/> class.
        /// </summary>
        /// <param name="iterations">Iterations value.</param>
        public TestsResults(ulong iterations)
        {
            this.Iterations = iterations;
            this.results = new List<AbstractResult>();
        }

        /// <summary>
        /// Gets the tests results value.
        /// </summary>
        public AbstractResult[] Results
        {
            get
            {
                return this.results.ToArray();
            }
        }

        /// <summary>
        /// Gets the number of iterations performed.
        /// </summary>
        public ulong Iterations { get; }

        /// <summary>
        /// Parse the current instance to a string value.
        /// </summary>
        /// <returns>A string formatted instance.</returns>
        public override string ToString()
        {
            return this.ParseToString();
        }

        /// <summary>
        /// Adds the elements of the specified collection to the end of result list.
        /// </summary>
        /// <param name="abstractResults">Result collection.</param>
        internal void AddRange(IEnumerable<AbstractResult> abstractResults)
        {
            this.results.AddRange(abstractResults);
        }
    }
}
