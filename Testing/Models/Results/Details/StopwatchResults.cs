﻿// <copyright file="StopwatchResults.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Models.Results.Details
{
    using System;

    /// <summary>
    /// This class contains time related test results.
    /// </summary>
    public class StopwatchResults
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StopwatchResults"/> class.
        /// </summary>
        /// <param name="totalTicksRegistered">Total ticks registered.</param>
        /// <param name="minimumTicksRegistered">Minimum ticks registered.</param>
        /// <param name="maximumTicksRegistered">Maximum ticks registered.</param>
        /// <param name="iterations">Number of iterations.</param>
        internal StopwatchResults(long totalTicksRegistered, long minimumTicksRegistered, long maximumTicksRegistered, ulong iterations)
        {
            this.TotalElapsed = (float)totalTicksRegistered / TimeSpan.TicksPerMillisecond;
            this.AverageRun = totalTicksRegistered / (float)iterations / TimeSpan.TicksPerMillisecond;
            this.ShortestRun = (float)minimumTicksRegistered / TimeSpan.TicksPerMillisecond;
            this.LongestRun = (float)maximumTicksRegistered / TimeSpan.TicksPerMillisecond;
        }

        /// <summary>
        /// Gets the total elapsed miliseconds value.
        /// </summary>
        public double TotalElapsed { get; }

        /// <summary>
        /// Gets the average elapsed miliseconds of a single iteration.
        /// </summary>
        public double AverageRun { get; }

        /// <summary>
        /// Gets the shortest elapsed miliseconds value.
        /// </summary>
        public double ShortestRun { get; }

        /// <summary>
        /// Gets the longest elapsed miliseconds value.
        /// </summary>
        public double LongestRun { get; }
    }
}
