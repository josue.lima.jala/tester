﻿// <copyright file="MemoryResults.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Models.Results.Details
{
    /// <summary>
    /// This class contains memory related test results.
    /// </summary>
    public class MemoryResults
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryResults"/> class.
        /// </summary>
        /// <param name="totalMemoryUsed">Total memory used.</param>
        /// <param name="minimumMemoryUsed">Minimum memory used.</param>
        /// <param name="maximumMemoryUsed">Maximum memory used.</param>
        /// <param name="iterations">Number of iterations.</param>
        internal MemoryResults(long totalMemoryUsed, long minimumMemoryUsed, long maximumMemoryUsed, ulong iterations)
        {
            this.TotalMemoryUsed = totalMemoryUsed;
            this.AverageMemoryUsed = totalMemoryUsed / (long)iterations;
            this.MinimumMemoryUsed = minimumMemoryUsed;
            this.MaximumMemoryUsed = maximumMemoryUsed;
        }

        /// <summary>
        /// Gets the total memory used value.
        /// </summary>
        public long TotalMemoryUsed { get; }

        /// <summary>
        /// Gets the average memory used by an iteration.
        /// </summary>
        public long AverageMemoryUsed { get; }

        /// <summary>
        /// Gets the minimum memory used value.
        /// </summary>
        public long MinimumMemoryUsed { get; }

        /// <summary>
        /// Gets the maximum memory used value.
        /// </summary>
        public long MaximumMemoryUsed { get; }
    }
}
