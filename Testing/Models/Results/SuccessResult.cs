﻿// <copyright file="SuccessResult.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Models.Results
{
    using System.Reflection;
    using Testing.Constants;
    using Testing.Models.Results.Details;

    /// <summary>
    /// This class contains a succesfull result information.
    /// </summary>
    public class SuccessResult : AbstractResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SuccessResult"/> class.
        /// </summary>
        /// <param name="methodInfo">Method info value.</param>
        /// <param name="memoryResults">Memory results information.</param>
        /// <param name="stopwatchResults">Stopwatch results information.</param>
        public SuccessResult(MethodInfo methodInfo, MemoryResults memoryResults, StopwatchResults stopwatchResults)
            : base(string.Format(InternalConstants.MessageFormat, methodInfo.DeclaringType.Name, methodInfo.Name))
        {
            this.MemoryResults = memoryResults;
            this.StopwatchResults = stopwatchResults;
        }

        /// <summary>
        /// Gets the memory results value.
        /// </summary>
        public MemoryResults MemoryResults { get; }

        /// <summary>
        /// Gets the time results value.
        /// </summary>
        public StopwatchResults StopwatchResults { get; }
    }
}
