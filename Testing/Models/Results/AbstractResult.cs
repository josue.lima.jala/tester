﻿// <copyright file="AbstractResult.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Models.Results
{
    using System.Text.RegularExpressions;
    using Testing.Constants;

    /// <summary>
    /// This class contains a result basic information.
    /// </summary>
    public abstract class AbstractResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractResult"/> class.
        /// </summary>
        /// <param name="sourceName">Source name value.</param>
        protected AbstractResult(string sourceName)
        {
            string formattedName = Regex.Replace(sourceName, InternalConstants.CapsLetterRegexFilter, InternalConstants.CapsLetterRegexReplacement, RegexOptions.Compiled).Trim();
            formattedName = Regex.Replace(formattedName, InternalConstants.UnderscoreRegexFilter, string.Empty);
            this.SourceName = formattedName;
        }

        /// <summary>
        /// Gets the source name value.
        /// </summary>
        public string SourceName { get; }
    }
}
