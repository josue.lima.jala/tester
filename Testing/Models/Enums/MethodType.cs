﻿// <copyright file="MethodType.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Models.Enums
{
    /// <summary>
    /// This enum is used to specify the tests run type.
    /// </summary>
    public enum MethodType : byte
    {
        /// <summary>
        /// Constructors.
        /// </summary>
        Instanciate,

        /// <summary>
        /// Prepare methods.
        /// </summary>
        Prepare,

        /// <summary>
        /// Run methods.
        /// </summary>
        Run,

        /// <summary>
        /// Clean methods.
        /// </summary>
        Clean,
    }
}