﻿// <copyright file="TestClass.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Models
{
    using System.Collections.Generic;
    using System.Reflection;
    using Testing.Models.Enums;

    /// <summary>
    /// This class contains information to run a single test.
    /// </summary>
    internal class TestClass
    {
        private readonly List<MethodInfo> prepareTestMethods;

        private readonly List<MethodInfo> cleanTestMethods;

        private readonly Dictionary<MethodType, List<MethodInfo>> methodDictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestClass"/> class.
        /// </summary>
        /// <param name="instance">Test class instance.</param>
        /// <param name="runTestMethod">Run test method.</param>
        internal TestClass(object instance, MethodInfo runTestMethod)
        {
            this.prepareTestMethods = new List<MethodInfo>();
            this.cleanTestMethods = new List<MethodInfo>();
            this.RunTestMethod = runTestMethod;
            this.Instance = instance;

            this.methodDictionary = new Dictionary<MethodType, List<MethodInfo>>
            {
                { MethodType.Prepare, this.prepareTestMethods },
                { MethodType.Clean, this.cleanTestMethods },
            };
        }

        /// <summary>
        /// Gets the run test method.
        /// </summary>
        internal MethodInfo RunTestMethod { get; }

        /// <summary>
        /// Gets the test class instance.
        /// </summary>
        internal object Instance { get; }

        /// <summary>
        /// This method will add the input method info to the corresponding collection based on the method type.
        /// </summary>
        /// <param name="methodInfo">Method info.</param>
        /// <param name="methodType">Method type.</param>
        internal void AddMethods(MethodInfo[] methodInfo, MethodType methodType)
        {
            this.methodDictionary.GetValueOrDefault(methodType).AddRange(methodInfo);
        }

        /// <summary>
        /// This method will return an array of methods corresponding to the input method type.
        /// </summary>
        /// <param name="methodType">Method type.</param>
        /// <returns>An array of method info.</returns>
        internal MethodInfo[] GetMethods(MethodType methodType)
        {
            return this.methodDictionary.GetValueOrDefault(methodType).ToArray();
        }
    }
}
