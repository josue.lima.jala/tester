﻿// <copyright file="Dictionaries.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Utilities
{
    using System.Collections.Generic;
    using Testing.Constants;
    using Testing.Models.Enums;

    /// <summary>
    /// This class contains dictionaries used by the library.
    /// </summary>
    internal static class Dictionaries
    {
        static Dictionaries()
        {
            MethodTypeDictionary = new Dictionary<MethodType, string>
            {
                { MethodType.Instanciate, InternalConstants.InstanciateOperationName },
                { MethodType.Prepare, InternalConstants.PrepareOperationName },
                { MethodType.Run, InternalConstants.RunOperationName },
                { MethodType.Clean, InternalConstants.CleanOperationName },
            };
        }

        /// <summary>
        /// Gets the method type dictionary.
        /// </summary>
        public static Dictionary<MethodType, string> MethodTypeDictionary { get; }
    }
}
