﻿// <copyright file="StringParser.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Utilities
{
    using System;
    using System.Text;
    using Testing.Constants;
    using Testing.Models.Results;

    /// <summary>
    /// This class contains the logic to parse an object to a string with all the object's fields and values.
    /// </summary>
    internal static class StringParser
    {
        /// <summary>
        /// This method will convert the input test result into a string.
        /// </summary>
        /// <param name="testsResults">Test result.</param>
        /// <returns>Result info.</returns>
        public static string ParseToString(this TestsResults testsResults)
        {
            StringBuilder stringBuilder = new StringBuilder();
            string iterationLine = string.Format(InternalConstants.TestFormat, StringParserConstants.Iterations, testsResults.Iterations);
            stringBuilder.Append(iterationLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Environment.NewLine);

            foreach (AbstractResult result in testsResults.Results)
            {
                AppendResult(stringBuilder, result);
            }

            return stringBuilder.ToString();
        }

        private static void AppendResult(StringBuilder stringBuilder, AbstractResult abstractResult)
        {
            if (abstractResult is ErrorResult)
            {
                AppendErrorResult(stringBuilder, abstractResult as ErrorResult);
            }

            if (abstractResult is SuccessResult)
            {
                AppendSuccessResult(stringBuilder, abstractResult as SuccessResult);
            }
        }

        private static void AppendSuccessResult(StringBuilder stringBuilder, SuccessResult successResult)
        {
            string nameLine = string.Format(InternalConstants.TestFormat, StringParserConstants.TestName, successResult.SourceName);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(nameLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.TimeElapsed);
            stringBuilder.Append(Environment.NewLine);
            string totalElapsed = string.Format(InternalConstants.TestFormat, StringParserConstants.TotalElapsed, successResult.StopwatchResults.TotalElapsed);
            string longestRun = string.Format(InternalConstants.TestFormat, StringParserConstants.LongestRun, successResult.StopwatchResults.LongestRun);
            string shortestRun = string.Format(InternalConstants.TestFormat, StringParserConstants.ShortestRun, successResult.StopwatchResults.ShortestRun);
            string averageRun = string.Format(InternalConstants.TestFormat, StringParserConstants.AverageRun, successResult.StopwatchResults.AverageRun);
            stringBuilder.Append(StringParserConstants.Identation);
            stringBuilder.Append(totalElapsed);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.Identation);
            stringBuilder.Append(longestRun);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.Identation);
            stringBuilder.Append(shortestRun);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.Identation);
            stringBuilder.Append(averageRun);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.MemoryUsed);
            stringBuilder.Append(Environment.NewLine);
            string totalBytesUsed = string.Format(InternalConstants.TestFormat, StringParserConstants.TotalUsed, successResult.MemoryResults.TotalMemoryUsed);
            string maximumBytesUsed = string.Format(InternalConstants.TestFormat, StringParserConstants.MaximumUsed, successResult.MemoryResults.MaximumMemoryUsed);
            string minimumBytesUsed = string.Format(InternalConstants.TestFormat, StringParserConstants.MinimumUsed, successResult.MemoryResults.MinimumMemoryUsed);
            string averageBytesUsed = string.Format(InternalConstants.TestFormat, StringParserConstants.AverageUsed, successResult.MemoryResults.AverageMemoryUsed);
            stringBuilder.Append(StringParserConstants.Identation);
            stringBuilder.Append(totalBytesUsed);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.Identation);
            stringBuilder.Append(maximumBytesUsed);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.Identation);
            stringBuilder.Append(minimumBytesUsed);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.Identation);
            stringBuilder.Append(averageBytesUsed);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Environment.NewLine);
        }

        private static void AppendErrorResult(StringBuilder stringBuilder, ErrorResult errorResult)
        {
            string nameLine = string.Format(InternalConstants.TestFormat, StringParserConstants.TestName, errorResult.SourceName);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(nameLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.Identation);
            string messageLine = string.Format(InternalConstants.TestFormat, StringParserConstants.Message, errorResult.Message);
            stringBuilder.Append(messageLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(StringParserConstants.Identation);
            string exceptionLine = string.Format(InternalConstants.TestFormat, StringParserConstants.Exception, errorResult.Exception);
            stringBuilder.Append(exceptionLine);
            stringBuilder.Append(Environment.NewLine);
        }
    }
}
