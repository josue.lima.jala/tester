﻿// <copyright file="ExtensionMethods.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Testing.Models.Enums;
    using Testing.Models.Results;

    /// <summary>
    /// A helper class containing extended methods.
    /// </summary>
    internal static class ExtensionMethods
    {
        /// <summary>
        /// This method will filter the types input with the generic attribute and return the result.
        /// </summary>
        /// <param name="types">Input array.</param>
        /// <typeparam name="TAttribute">The attribute type to filter the array.</typeparam>
        /// <returns>A filtered type array.</returns>
        internal static Type[] FilterWithAttribute<TAttribute>(this Type[] types)
            where TAttribute : Attribute
        {
            List<Type> filteredResult = new List<Type>();
            Type attributeType = typeof(TAttribute);

            foreach (Type type in types)
            {
                if (type.GetCustomAttributes(attributeType, true).Any())
                {
                    filteredResult.Add(type);
                }
            }

            return filteredResult.ToArray();
        }

        /// <summary>
        /// This method will filter the method info array input with the generic attribute and return the result.
        /// </summary>
        /// <param name="methods">Input array.</param>
        /// <typeparam name="TAttribute">The attribute type to filter the array.</typeparam>
        /// <returns>A filtered MethodInfo array.</returns>
        internal static MethodInfo[] FilterWithAttribute<TAttribute>(this MethodInfo[] methods)
            where TAttribute : Attribute
        {
            List<MethodInfo> filteredResult = new List<MethodInfo>();

            foreach (MethodInfo method in methods)
            {
                var attribute = method.GetCustomAttribute<TAttribute>();

                if (attribute != null)
                {
                    filteredResult.Add(method);
                }
            }

            return filteredResult.ToArray();
        }

        /// <summary>
        /// This method will generate an array of instances based on the input types and add an error result to the input collection.
        /// </summary>
        /// <param name="types">Input array.</param>
        /// <param name="errorResults">An array of abstract results.</param>
        /// <returns>An instance array.</returns>
        /// <typeparam name="TObject">Intance type.</typeparam>
        internal static TObject[] GenerateInstances<TObject>(this IEnumerable<Type> types, List<ErrorResult> errorResults)
        {
            List<TObject> instances = new List<TObject>();

            foreach (Type type in types)
            {
                instances.AddRange(GenerateInstance<TObject>(errorResults, type));
            }

            return instances.ToArray();
       }

        private static TObject[] GenerateInstance<TObject>(List<ErrorResult> errorResults, Type type)
        {
            List<TObject> instances = new List<TObject>();

            try
            {
                object instance = null;
                instance = Activator.CreateInstance(type);
                instances.Add((TObject)instance);
            }
            catch (MissingMethodException)
            {
                ErrorResult error = new ErrorResult(type);
                errorResults.Add(error);
            }
            catch (TargetInvocationException ex)
            {
                ErrorResult error = new ErrorResult(ex.InnerException, MethodType.Instanciate);
                errorResults.Add(error);
            }

            return instances.ToArray();
        }
    }
}
