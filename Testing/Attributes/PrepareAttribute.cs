﻿// <copyright file="PrepareAttribute.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Main.Attributes
{
    using System;

    /// <summary>
    /// This attribute class is used to retrieve all prepare methods.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class PrepareAttribute : Attribute
    {
    }
}
