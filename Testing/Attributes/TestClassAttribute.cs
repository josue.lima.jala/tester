﻿// <copyright file="TestClassAttribute.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Main.Attributes
{
    using System;

    /// <summary>
    /// This attribute class is used to retrieve all test classes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class TestClassAttribute : Attribute
    {
    }
}
