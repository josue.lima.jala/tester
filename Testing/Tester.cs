﻿// <copyright file="Tester.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing
{
    using System.Collections.Generic;
    using Testing.Managers;
    using Testing.Models.Results;

    /// <summary>
    /// Main program class.
    /// </summary>
    public static class Tester
    {
        /// <summary>
        /// This method will run the tests.
        /// </summary>
        /// <param name="iterations">Iterations value.</param>
        /// <returns>Test results.</returns>
        public static TestsResults Run(ulong iterations)
        {
            TestsResults results = new TestsResults(iterations);
            AbstractResult[] resultsCollection = GetResultsCollection(iterations);
            results.AddRange(resultsCollection);

            return results;
        }

        private static AbstractResult[] GetResultsCollection(ulong iterations)
        {
            List<AbstractResult> results = new List<AbstractResult>();
            TestManager testManager = new TestManager();
            AbstractResult[] managerResults = testManager.Run(iterations);
            results.AddRange(managerResults);

            return results.ToArray();
        }
    }
}
