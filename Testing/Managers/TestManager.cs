﻿// <copyright file="TestManager.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Managers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using Testing.Main.Attributes;
    using Testing.Models;
    using Testing.Models.Enums;
    using Testing.Models.Results;
    using Testing.Models.Results.Details;
    using Testing.Utilities;

    /// <summary>
    /// This class is used to perform test operations that were tagged with the test attribute.
    /// </summary>
    internal sealed class TestManager
    {
        private readonly Stopwatch stopwatch;

        private readonly List<TestClass> testClasses;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestManager"/> class.
        /// </summary>
        public TestManager()
        {
            this.stopwatch = new Stopwatch();
            this.testClasses = new List<TestClass>();
        }

        /// <summary>
        /// This method will instanciate the test classes, run the code and then it will return the test results.
        /// </summary>
        /// <param name="iterations">Number of iterations.</param>
        /// <returns>A results array.</returns>
        internal AbstractResult[] Run(ulong iterations)
        {
            List<AbstractResult> results = new List<AbstractResult>();
            results.AddRange(this.InstanciateTestClasses(Assembly.GetEntryAssembly().GetTypes()));
            results.AddRange(this.RunTestClasses(iterations));

            return results.ToArray();
        }

        private ErrorResult[] InstanciateTestClasses(Type[] types)
        {
            List<ErrorResult> errorResults = new List<ErrorResult>();
            Type[] testTypes = types.FilterWithAttribute<TestClassAttribute>();
            object[] instances = testTypes.GenerateInstances<object>(errorResults);
            this.testClasses.AddRange(this.CreateTestClassesFromInstances(instances));

            return errorResults.ToArray();
        }

        private IEnumerable<AbstractResult> RunTestClasses(ulong iterations)
        {
            foreach (TestClass testClass in this.testClasses)
            {
                AbstractResult testClassResult = this.SingleRun(testClass, iterations);
                yield return testClassResult;
            }
        }

        private ErrorResult SingleOperation(TestClass testClass, MethodType methodType)
        {
            foreach (MethodInfo classMethod in testClass.GetMethods(methodType))
            {
                try
                {
                    classMethod.Invoke(testClass.Instance, null);
                }
                catch (Exception ex)
                {
                    return new ErrorResult(ex, classMethod, methodType);
                }
            }

            return null;
        }

        private AbstractResult SingleRun(TestClass testClass, ulong iterations)
        {
            AbstractResult result;

            try
            {
                result = this.TrySingleRun(testClass, iterations);
            }
            catch (Exception ex)
            {
                result = new ErrorResult(ex.InnerException, testClass.RunTestMethod, MethodType.Run);
            }
            finally
            {
                if (this.stopwatch.IsRunning)
                {
                    this.stopwatch.Stop();
                }
            }

            return result;
        }

        private AbstractResult TrySingleRun(TestClass testClass, ulong iterations)
        {
            long totalTicks = default;
            long minimumTicks = long.MaxValue;
            long maximumTicks = long.MinValue;

            long totalAllocatedBytes = default;
            long minimumBytes = long.MaxValue;
            long maximumBytes = long.MinValue;

            for (ulong i = default; i < iterations; i++)
            {
                ErrorResult prepareResult = this.SingleOperation(testClass, MethodType.Prepare);

                if (prepareResult != null)
                {
                    return prepareResult;
                }

                long resultAllocatedBytes;
                this.stopwatch.Restart();
                long startMemory = GC.GetTotalAllocatedBytes(true);
                testClass.RunTestMethod.Invoke(testClass.Instance, null);
                long endMemory = GC.GetTotalAllocatedBytes(true);
                this.stopwatch.Stop();
                totalTicks += this.stopwatch.ElapsedTicks;
                resultAllocatedBytes = endMemory - startMemory;
                totalAllocatedBytes += resultAllocatedBytes;

                if (this.stopwatch.ElapsedTicks < minimumTicks)
                {
                    minimumTicks = this.stopwatch.ElapsedTicks;
                }

                if (this.stopwatch.ElapsedTicks > maximumTicks)
                {
                    maximumTicks = this.stopwatch.ElapsedTicks;
                }

                if (resultAllocatedBytes < minimumBytes)
                {
                    minimumBytes = resultAllocatedBytes;
                }

                if (resultAllocatedBytes > maximumBytes)
                {
                    maximumBytes = resultAllocatedBytes;
                }

                ErrorResult cleanResult = this.SingleOperation(testClass, MethodType.Clean);

                if (cleanResult != null)
                {
                    return cleanResult;
                }
            }

            StopwatchResults stopwatchResult = new StopwatchResults(totalTicks, minimumTicks, maximumTicks, iterations);
            MemoryResults memoryResult = new MemoryResults(totalAllocatedBytes, minimumBytes, maximumBytes, iterations);

            return new SuccessResult(testClass.RunTestMethod, memoryResult, stopwatchResult);
        }

        private TestClass[] CreateTestClassesFromInstances(object[] instances)
        {
            List<TestClass> testClasses = new List<TestClass>();

            foreach (object instance in instances)
            {
                testClasses.AddRange(this.CreateTestClassesFromInstance(instance));
            }

            return testClasses.ToArray();
        }

        private TestClass[] CreateTestClassesFromInstance(object instance)
        {
            List<TestClass> testClasses = new List<TestClass>();
            Type type = instance.GetType();
            MethodInfo[] instanceMethods = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo[] prepareMethods = instanceMethods.FilterWithAttribute<PrepareAttribute>();
            MethodInfo[] runMethods = instanceMethods.FilterWithAttribute<RunAttribute>();
            MethodInfo[] cleanMethods = instanceMethods.FilterWithAttribute<CleanAttribute>();

            foreach (MethodInfo runMethod in runMethods)
            {
                TestClass testClass = new TestClass(instance, runMethod);
                testClass.AddMethods(prepareMethods, MethodType.Prepare);
                testClass.AddMethods(cleanMethods, MethodType.Clean);

                testClasses.Add(testClass);
            }

            return testClasses.ToArray();
        }
    }
}
