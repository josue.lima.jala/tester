﻿// <copyright file="StringParserConstants.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Constants
{
    /// <summary>
    /// This class contains string constants used to parse a Test Result to a string value.
    /// </summary>
    internal static class StringParserConstants
    {
        /// <summary>
        /// Iteration value.
        /// </summary>
        internal const string Iterations = "Iterations";

        /// <summary>
        /// Test name value.
        /// </summary>
        internal const string TestName = "TestName";

        /// <summary>
        /// Time values title.
        /// </summary>
        internal const string TimeElapsed = "Time results in miliseconds";

        /// <summary>
        /// Total time elapsed.
        /// </summary>
        internal const string TotalElapsed = "Total elapsed";

        /// <summary>
        /// Longest time elapsed.
        /// </summary>
        internal const string LongestRun = "Longest run";

        /// <summary>
        /// Shortest time elapsed.
        /// </summary>
        internal const string ShortestRun = "Shortest run";

        /// <summary>
        /// Average run elapsed.
        /// </summary>
        internal const string AverageRun = "Average run";

        /// <summary>
        /// Identation value.
        /// </summary>
        internal const string Identation = "- ";

        /// <summary>
        /// Memory values title.
        /// </summary>
        internal const string MemoryUsed = "Memory bytes used";

        /// <summary>
        /// Total memory used.
        /// </summary>
        internal const string TotalUsed = "Total used";

        /// <summary>
        /// Maximum memory used.
        /// </summary>
        internal const string MaximumUsed = "Maximum used";

        /// <summary>
        /// Minimum memory used.
        /// </summary>
        internal const string MinimumUsed = "Minimum used";

        /// <summary>
        /// Average memory used.
        /// </summary>
        internal const string AverageUsed = "Average used";

        /// <summary>
        /// Memory message.
        /// </summary>
        internal const string Message = "Message";

        /// <summary>
        /// Exception message.
        /// </summary>
        internal const string Exception = "Exception";
    }
}
