﻿// <copyright file="InternalConstants.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Constants
{
    /// <summary>
    /// Constants used to perform internal logic.
    /// </summary>
    internal static class InternalConstants
    {
        /// <summary>
        /// Instanciate operation name.
        /// </summary>
        internal const string InstanciateOperationName = "instanciating";

        /// <summary>
        /// Prepare operation name.
        /// </summary>
        internal const string PrepareOperationName = "preparing";

        /// <summary>
        /// Prepare method name.
        /// </summary>
        internal const string PrepareMethodName = "Prepare";

        /// <summary>
        /// Run operation name.
        /// </summary>
        internal const string RunOperationName = "running";

        /// <summary>
        /// Run method name.
        /// </summary>
        internal const string RunMethodName = "Run";

        /// <summary>
        /// Retrieving methods operation name.
        /// </summary>
        internal const string RetrievingMethodsOperationName = "retrieving the methods from the";

        /// <summary>
        /// Clean operation name.
        /// </summary>
        internal const string CleanOperationName = "cleaning";

        /// <summary>
        /// Clean method name.
        /// </summary>
        internal const string CleanMethodName = "Clean";

        /// <summary>
        /// String format used by the StringParser to format the string output.
        /// </summary>
        internal const string TestFormat = "{0,-15}{1}";

        /// <summary>
        /// String format used format a test name.
        /// </summary>
        internal const string MessageFormat = "{0} -{1}.";

        /// <summary>
        /// Regex filter used to filter caps letters.
        /// </summary>
        internal const string CapsLetterRegexFilter = "([A-Z])";

        /// <summary>
        /// Regex filter used to filter underscores.
        /// </summary>
        internal const string UnderscoreRegexFilter = "(_)";

        /// <summary>
        /// Regex replacement used to replace a value.
        /// </summary>
        internal const string CapsLetterRegexReplacement = " $1";
    }
}
