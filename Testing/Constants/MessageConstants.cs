﻿// <copyright file="MessageConstants.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Constants
{
    /// <summary>
    /// Constants used to create messages.
    /// </summary>
    internal static class MessageConstants
    {
        /// <summary>
        /// Message that will be shown when no results are found.
        /// </summary>
        internal const string NoResultsFoundMessage = "No results were found.";

        /// <summary>
        /// Message that will be shown when a class can't be instanciated.
        /// </summary>
        internal const string NonInvocableMessage = "A test class must have a public parameterless constructor to be instanciated.";

        /// <summary>
        /// Message that will be shown when a target method throws an exception.
        /// </summary>
        internal const string TargetInvocationMessage = "Something went wrong while {0} the class.";

        /// <summary>
        /// Message that will be shown when a unhandled exception is thrown.
        /// </summary>
        internal const string InternalErrorMessage = "Something went wrong with the library.";
    }
}