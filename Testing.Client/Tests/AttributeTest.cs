﻿// <copyright file="AttributeTest.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Client.TestClasses.Tests
{
    using System;
    using System.Diagnostics;
    using Testing.Main.Attributes;

    /// <summary>
    /// A test class.
    /// </summary>
    [TestClass]
    public class AttributeTest
    {
        private readonly Stopwatch stopwatch;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeTest"/> class.
        /// </summary>
        public AttributeTest()
        {
            this.stopwatch = new Stopwatch();
        }

        /// <summary>
        /// Run method.
        /// </summary>
        [Run]
        public void CollectBytes()
        {
            GC.GetTotalAllocatedBytes(true);
            int x = 0;
            _ = 1 / x;
        }

        /// <summary>
        /// Another run method.
        /// </summary>
        [Run]
        public void RunStopwatch()
        {
            this.stopwatch.Restart();
            this.stopwatch.Stop();
        }

        /// <summary>
        /// Run method.
        /// </summary>
        [Run]
        public void EmptyMethod()
        {
        }
    }
}
