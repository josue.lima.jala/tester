﻿// <copyright file="MessageConstants.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Client.Constants
{
    /// <summary>
    /// Response message constants.
    /// </summary>
    internal static class MessageConstants
    {
        /// <summary>
        /// Message that will be shown when asking the user for a iteration value.
        /// </summary>
        internal const string ReadValue = "Insert the number of iterations for this test, an empty input or a 0 value will be considered as 20 by default.";

        /// <summary>
        /// Message that will be shown when the user input iteration value is not a numeric value.
        /// </summary>
        internal const string NonNumericInput = "The input is not a positive numeric value, running with default value...";

        /// <summary>
        /// Message that will be shown when the user input iteration value is equal to zero.
        /// </summary>
        internal const string CantBeZero = "The input must be greather than 0, running with default value...";
    }
}
