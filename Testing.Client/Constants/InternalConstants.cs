﻿// <copyright file="InternalConstants.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Client.Constants
{
    /// <summary>
    /// Constants used to perform internal logic.
    /// </summary>
    internal static class InternalConstants
    {
        /// <summary>
        /// Default iterations value.
        /// </summary>
        internal const int DefaultIterations = 20;
    }
}
