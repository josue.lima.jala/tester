﻿// <copyright file="Program.cs" company="Josue Lima">
// Copyright (c) Josue Lima. All rights reserved.
// </copyright>

namespace Testing.Client
{
    using System;
    using Testing.Client.Constants;
    using Testing.Models.Results;

    /// <summary>
    /// Main class.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main program method.
        /// </summary>
        public static void Main()
        {
            ulong iterations = ReadInputIterations();
            TestsResults results = Tester.Run(iterations);
            PrintResults(results);

            Console.ReadKey();
        }

        /// <summary>
        /// This method will print a result collection messages.
        /// </summary>
        /// <param name="results">Results collection.</param>
        private static void PrintResults(TestsResults results)
        {
            Console.Clear();
            string stringResult = results.ToString();
            Print(stringResult);
        }

        /// <summary>
        /// This method will read a numeric value from the console.
        /// </summary>
        /// <returns>Input numeric value.</returns>
        private static ulong ReadInputIterations()
        {
            Print(MessageConstants.ReadValue);
            string input = Console.ReadLine();

            return ValidateInput(input);
        }

        /// <summary>
        /// This method will validate if the input value is a valid positive number greather than zero.
        /// If not, it will return a default value from the constants.
        /// </summary>
        /// <param name="input">User input value.</param>
        /// <returns>A positive numeric result.</returns>
        private static ulong ValidateInput(string input)
        {
            bool isNumber = ulong.TryParse(input, out ulong iterations);

            if (!isNumber)
            {
                Print(MessageConstants.NonNumericInput);
                return InternalConstants.DefaultIterations;
            }

            if (iterations == 0)
            {
                Print(MessageConstants.CantBeZero);
                return InternalConstants.DefaultIterations;
            }

            return iterations;
        }

        /// <summary>
        /// This method will print on the console the required message.
        /// </summary>
        /// <param name="message">Message to print.</param>
        /// <param name="args">Message optioanl args.</param>
        private static void Print(string message, params object[] args)
        {
            Console.WriteLine(message, args);
        }
    }
}
